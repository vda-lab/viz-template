# Template for visualization projects

Template for visualization projects, based on example Moritz Stefaner

## Starting a new project

    git clone --depth 1 --origin source https://github.com/jandot/viz-template.git new-project
    cd new-project
    hub create
    git remote rm source
    git push --set-upstream origin master

## Setting things up

* Install live reload plugin in Chrome, and make sure it is *on* (middle circle should be full).
* Install necessary grunt plugins
 

    npm install grunt-contrib-connect --save-dev
    npm install grunt-contrib-coffee --save-dev
    npm install grunt-contrib-watch --save-dev

* Edit src/main.coffe

MIT License (c) 2014 Jan Aerts

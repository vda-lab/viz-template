module.exports = (grunt) ->
  grunt.initConfig
    connect:
      server:
        port: 8000
        # base: './web'
    coffee:
      compile:
        files:
          'lib/main.js': ['src/*.coffee']
    watch:
      scripts:
        files: ['src/*.coffee', 'index.html']
        tasks: ['coffee']
        options:
          livereload: true

  grunt.loadNpmTasks 'grunt-contrib-connect'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  grunt.registerTask 'default', ['connect', 'watch']